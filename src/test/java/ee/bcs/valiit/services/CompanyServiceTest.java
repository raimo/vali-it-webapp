package ee.bcs.valiit.services;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ee.bcs.valiit.model.Company;

public class CompanyServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws SQLException {
		ResultSet result = CompanyService.executeSQL("select * from company");
		assertNotNull(result);
		assertTrue(result.next()); // kas on veel järgmine rida?
		int id = result.getInt("id");
		assertTrue(id > 0);
		String name = result.getString("name");
		assertTrue(name.length() > 0);
	}

	@Test
	public void testGetCompanies() {
		List<Company> companies = CompanyService.getCompanies();
		assertTrue(companies.size() > 0);
		assertTrue(companies.get(0).getName().length() > 0);
	}

}
