package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.model.Company;

public class CompanyService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/companies";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSQL(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Company> getCompanies() {
		List<Company> companies = new ArrayList<Company>(); // loon listi, mis koosneb objektisest, mille tüüp on
															// Company
		try {
			ResultSet result = executeSQL("select * from company");
			if (result != null) {
				while (result.next()) { // kuni saan liikuda järgmisele reale
					Company company = new Company(); // loon iga kord Company objekti ja määran selle parameetritele
														// väärtused
					company.setId(result.getInt("id"));
					company.setName(result.getString("name"));
					company.setEmployeeCount(result.getInt("employee_count"));
					company.setEstablished(result.getString("established"));
					company.setLogo(result.getString("logo"));
					companies.add(company); // lisan loodud objekti companies listi (objektidest)
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return companies;
	}

	public static void addCompany(Company company) {
		// Salvestame ettevõtte andmebaasi

		if (!companyExists(company.getName())) {
			String sql = "INSERT INTO company (name, employee_count, established, logo) VALUES ('%s', %s, '%s', '%s')";
			sql = String.format(sql, company.getName(), company.getEmployeeCount(), company.getEstablished(),
					company.getLogo());
			executeSQL(sql);
		}
	}

	public static void removeCompany(int companyID) {
		String sql = "DELETE FROM company WHERE id = " + companyID;
		executeSQL(sql);
	}

	public static boolean companyExists(String name) {
		boolean companyExists = false;
		try {
			ResultSet result = executeSQL("select id from company where name = '" + name + "'");
			if (result != null) {
				companyExists = result.next();

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return companyExists;
	}

	public static Company getCompany(int iD) {
		try {
			String sql = "select * from company where id=" + iD;
			ResultSet result = executeSQL(sql);
			if (result != null) {
				if (result.next()) { // kuni saan liikuda järgmisele reale
					Company company = new Company(); // loon iga kord Company objekti ja määran selle parameetritele
														// väärtused
					company.setId(result.getInt("id"));
					company.setName(result.getString("name"));
					company.setEmployeeCount(result.getInt("employee_count"));
					company.setEstablished(result.getString("established"));
					company.setLogo(result.getString("logo"));
					return company;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static void editCompany(Company company) {
		String sql = String.format("UPDATE company SET name= '%s', employee_count = %s, established ='%s', logo = '%s' WHERE id=%s",
				company.getName(), company.getEmployeeCount(), company.getEstablished(), company.getLogo(), company.getId());
		executeSQL(sql);
	}
}
